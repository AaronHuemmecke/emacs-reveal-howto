#+REVEAL_HTML: <div class="slide-footer"><br></div></section><section id="slide-license" data-state="no-toc-progress"><h3 class="no-toc-progress">License Information</h3>
@@latex:\section*{Lizenzinformation}@@

Solange nicht explizit anders angegeben ist dieses Werk
@@html:“<span property="dc:title">@@{{{title}}}@@html:</span>@@”
von
@@html:<span property="dc:creator cc:attributionName">@@{{{author}}}@@html:</span>@@
unter der Creative Commons Lizenz @@latex: \href{https://creativecommons.org/licenses/by-sa/4.0/}{CC BY-SA 4.0.}@@
@@html:<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0.</a>@@ veröffentlicht.